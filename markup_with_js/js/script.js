

// TABS

const servicesTabs = Array.from(document.querySelectorAll('.services-tabs-title'));
const servicesItems = Array.from(document.querySelectorAll('.services-item'));
const container = document.querySelector('.services-tabs');


container.addEventListener('click', event => {
    servicesTabs.forEach(item => {
        item.classList.remove('show');
    });
    event.target.classList.add('show');

    servicesItems.forEach(item => {
        item.classList.remove('show');
        if (event.target.dataset.vab === item.dataset.vab) {
            item.classList.add('show');
        }
    })
});



// WORK

const workTabs = document.querySelectorAll('.work-tabs-title');
const workImg = document.querySelectorAll('.work-img');
const workContainer = document.querySelector('.work-tabs');
const workBtn = document.querySelector('.work-button');


workContainer.addEventListener('click', event => {
    workTabs.forEach(item => {
        item.classList.remove('active');
    });

    event.target.classList.add('active');

    workImg.forEach(item => {
        item.classList.remove('active');
        if (event.target.dataset.tab === item.dataset.tab || event.target.dataset.lab === item.dataset.lab) {
            item.classList.add('active');
        }
    });

});

workBtn.addEventListener("click", () => {
    workImg.forEach(item => {
        item.classList.remove('hidden');
        item.classList.add('active');
        workBtn.hidden = true;

    })
});


// SLIDER

$(document).ready(function(){
    $('.slider-1').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-2'
    });
});

$('.slider-2').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-1',
    arrows: true,
    prevArrow: $('.prev'),
    nextArrow: $('.next'),
    focusOnSelect: true

});

const photo = document.querySelectorAll('.reviews-photo-small');
const photoContainer = document.querySelector('.slider-2');
const arrowContainer = document.querySelector('.slider-arrows');

photoContainer.addEventListener('click', event => {
    photo.forEach(item => {
        item.classList.remove('focus');
    });
    event.target.classList.add('focus')});

arrowContainer.addEventListener('click', event => {
    photo.forEach(item => {
        item.classList.remove('focus');
    });
});
